﻿using NUnit.Framework;
using System;
using SeleniumTraining.PageObjects;

namespace SeleniumTrainingTests
{
    [TestFixture]
    public class LoginTest
    {
        ApiLogin login { get; set; }

        [SetUp]
        public void SetupTest()
        {
            login = new ApiLogin("Firefox");
        }

        [TearDown]
        public void TeardownTest()
        {
            this.login.QuitDriver();
        }

        [Test]
        public void StaffCanLoginTest()
        {
            var username = "prueba";
            var password = "training1";
            login.GoTo();
            login.InputUsername(username);
            login.InputPassword(password);
            login.PressSubmit();
            Assert.IsTrue(login.isAtHome(), "Not on home");
        }

        [Test]
        public void NonStaffCantLoginTest()
        {
            var username = "nonstaff";
            var password = "training1";
            login.GoTo();
            login.InputUsername(username);
            login.InputPassword(password);
            login.PressSubmit();
            Assert.IsTrue(login.isAtLogin(), "Not on login");
        }
    }
}