﻿using SeleniumTraining.PageObjects;
using SeleniumTraining;
using NUnit.Framework;
using System;

namespace SeleniumTrainingTests
{
    [TestFixture]
    public class TaxactTest
    {
        TaxactHome taxact { get; set; }

        [SetUp]
        public void SetupTest()
        {
            taxact = new TaxactHome("Firefox");
        }

        [TearDown]
        public void TeardownTest()
        {
            this.taxact.QuitDriver();
        }

        [Test]
        public void NoRecommendedProductWhenFirstOpenTest()
        {
            taxact.GoTo();
            taxact.GoToTaxesOnline();
            bool isThereRecommended = taxact.IsThereRecommendedProductInTaxesOnline();
            Assert.IsFalse(isThereRecommended, "There's a recommended product.");
        }

        [Test]
        public void NoRecommendedProductWhenNoItemSelectedTest()
        {
            taxact.GoTo();
            taxact.GoToTaxesOnline();
            taxact.SelectLifeEventInTaxesOnline(LifeEvents.CHILD);
            taxact.DeselectLifeEventInTaxesOnline(LifeEvents.CHILD);
            bool isThereRecommended = taxact.IsThereRecommendedProductInTaxesOnline();
            Assert.IsFalse(isThereRecommended, "There's a recommended product.");

        }

        [TestCase(LifeEvents.JOB, OnlineProducts.FREE)]
        [TestCase(LifeEvents.CHILD, OnlineProducts.BASIC)]
        [TestCase(LifeEvents.HOME, OnlineProducts.DELUXE)]
        [TestCase(LifeEvents.INVESTMENT, OnlineProducts.PREMIER)]
        [TestCase(LifeEvents.SELFEMPLOYED, OnlineProducts.SELFEMPLOYED)]
        public void RecommendedProductFromSingleLifeEventTest(string lifeEvent, string recommended)
        {
            taxact.GoTo();
            taxact.GoToTaxesOnline();
            taxact.SelectLifeEventInTaxesOnline(lifeEvent);
            var recommendedProduct = taxact.GetRecommendedProduct();
            Assert.AreEqual(recommendedProduct, recommended);
        }

        [Test]
        public void RightProductRecommendedWhenMultipleRandomLifeEventsSelectedTest()
        {
            Random rnd = new Random();
            int rnd1 = rnd.Next(0, 4);
            int rnd2 = rnd.Next(0, 4);
            taxact.GoTo();
            taxact.GoToTaxesOnline();
            taxact.SelectLifeEventInTaxesOnline(LifeEvents.Events[rnd1]);
            taxact.SelectLifeEventInTaxesOnline(LifeEvents.Events[rnd2]);
            var shouldRecommend = OnlineProducts.Products[Math.Max(rnd1, rnd2)];
            var recommendedProduct = taxact.GetRecommendedProduct();
            Assert.AreEqual(recommendedProduct, shouldRecommend);
        }

    }
}
