﻿using System;
using SeleniumTraining.PageObjects;
using NUnit.Framework;

namespace SeleniumTrainingTests
{
    [TestFixture]
    public class ExplicitWaitTest
    {
        UiPracticeStudentsContact studentsContact { get; set; }

        [SetUp]
        public void SetupTest()
        {
            studentsContact = new UiPracticeStudentsContact("Firefox");
        }

        [TearDown]
        public void TeardownTest()
        {
            this.studentsContact.QuitDriver();
        }

        [Test]
        public void ExplicitWaitTest_First()
        {
            studentsContact.GoTo();
            studentsContact.ClickAjaxLink();
            Assert.AreEqual(true, studentsContact.GetContactUsContent().Contains("Python"), "ContactUs not found");
        }
    }
}
