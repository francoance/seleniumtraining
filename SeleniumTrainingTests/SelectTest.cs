﻿using System.Collections.Generic;
using SeleniumTraining.PageObjects;
using NUnit.Framework;

namespace SeleniumTrainingTests
{
    [TestFixture]
    public class SelectTest
    {
        public StudentsSelect studentsSelect { get; set; }

        [SetUp]
        public void SetupTest()
        {
            studentsSelect = new StudentsSelect("Firefox");
        }

        [TearDown]
        public void TeardownTest()
        {
            studentsSelect.QuitDriver();
        }

        [Test]
        public void CanSelectSingleTest()
        {
            string valueToSelect = "England";
            studentsSelect.GoTo();
            studentsSelect.SelectSingle(valueToSelect);
            var singleSelectValue = studentsSelect.GetSingleSelectValue();
            Assert.AreEqual(valueToSelect, singleSelectValue, "Single select picked the wrong value");
        }

        [Test]
        public void CanSelectMultiTest()
        {
            List<string> valuesToSelect = new List<string> { "England", "China" };
            studentsSelect.GoTo();
            studentsSelect.SelectMulti(valuesToSelect);
            List<string> multiSelectValues = studentsSelect.GetMultiSelectValues();
            var set = new HashSet<string>(valuesToSelect);
            Assert.AreEqual(true, set.SetEquals(multiSelectValues), "Multi select picked the wrong value");
        }

        [Test]
        public void CanSelectDropdownTest()
        {
            string valueToSelect = "England";
            studentsSelect.GoTo();
            studentsSelect.SelectDropdown(valueToSelect);
            var dropdownValue = studentsSelect.GetDropdownValue();
            Assert.AreEqual(valueToSelect, dropdownValue, "Dropdown picked the wrong value");
        }
    }
}
