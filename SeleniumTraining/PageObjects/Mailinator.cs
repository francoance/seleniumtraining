﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTraining.PageObjects
{
    public class Mailinator
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait Wait;
        private readonly string url = "https://www.mailinator.com";

        public Mailinator(string browser)
        {
            this.driver = WebdriverFactory.GetWebdriver(browser);
            this.Wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(30));
            PageFactory.InitElements(this.driver, this);
        }

        [FindsBy(How = How.Id, Using = "inboxfield")]
        public IWebElement InboxField { get; set; }

        [FindsBy(How = How.ClassName, Using = "btn btn-dark")]
        public IWebElement InboxGoButton { get; set; }

        [FindsBy(How = How.ClassName, Using = "table table-striped jambo-table")]
        public IWebElement MailsTable { get; set; }

        [FindsBy(How = How.Id, Using = "msgpane")]
        public IWebElement MsgPane { get; set; }

        public void GoTo()
        {
            this.driver.Navigate().GoToUrl(this.url);
        }

        public void QuitDriver()
        {
            this.driver.Quit();
        }

        public string GetMailTitle()
        {
            var x_title = MsgPane.FindElement(By.ClassName("x_title"));
            var mail_title = x_title.FindElement(By.TagName("h2"));
            return mail_title.Text;
        }
    }
}
