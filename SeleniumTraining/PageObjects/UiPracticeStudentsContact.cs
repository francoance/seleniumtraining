﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTraining.PageObjects
{
    public class UiPracticeStudentsContact
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait Wait;
        private readonly string url = "http://uitestpractice.com/Students/Contact";

        public UiPracticeStudentsContact(string browser)
        {
            this.driver = WebdriverFactory.GetWebdriver(browser);
            this.Wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(30));
            PageFactory.InitElements(this.driver, this);
        }

        [FindsBy(How = How.LinkText, Using = "This is a Ajax link")]
        public IWebElement AjaxLink { get; set; }

        public void GoTo()
        {
            this.driver.Navigate().GoToUrl(url);
        }

        public void QuitDriver()
        {
            this.driver.Quit();
        }

        public void ClickAjaxLink()
        {
            this.AjaxLink.Click();
        }

        public string GetContactUsContent()
        {
            var ContactUsDiv = this.Wait.Until<IWebElement>((d) =>
             {
                 return d.FindElement(By.ClassName("ContactUs"));
             }
            );
            return ContactUsDiv.Text;
        }
    }
}
