﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;

namespace SeleniumTraining.PageObjects
{
    public class TaxactAuth
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait Wait;
        private readonly string url = "https://aws-test.taxact.com";

        public TaxactAuth(string browser)
        {
            this.driver = WebdriverFactory.GetWebdriver(browser);
            this.Wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(30));
            PageFactory.InitElements(this.driver, this);
        }

        [FindsBy(How = How.LinkText, Using = "Create an Account")]
        public IWebElement CreateAccountButton { get; set; }

        [FindsBy(How = How.Id, Using = "Email")]
        public IWebElement EmailField { get; set; }

        [FindsBy(How = How.ClassName, Using = "btn btn-primary auth-forward")]
        public IWebElement NextButton { get; set; }

        [FindsBy(How = How.ClassName, Using = "btn btn-primary tab-forward")]
        public IWebElement ValidateButton { get; set; }

        [FindsBy(How = How.Id, Using = "btnAccept")]
        public IWebElement AcceptButton { get; set; }

        [FindsBy(How = How.LinkText, Using = "Skip for now")]
        public IWebElement SkipforNowLink { get; set; }

        [FindsBy(How = How.ClassName, Using = "confirmation-code")]
        public IWebElement ConfirmationCodeList { get; set; }

        [FindsBy(How = How.Id, Using = "Username")]
        public IWebElement UsernameInput { get; set; }

        [FindsBy(How = How.Id, Using = "Password")]
        public IWebElement PasswordInput { get; set; }

        [FindsBy(How = How.Id, Using = "PhoneNumber")]
        public IWebElement PhoneNumberInput { get; set; } //11 numbers

        public void GoTo()
        {
            this.driver.Navigate().GoToUrl(url);
        }

        public void QuitDriver()
        {
            this.driver.Quit();
        }

    }
}
