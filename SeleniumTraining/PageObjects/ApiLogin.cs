﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;

namespace SeleniumTraining.PageObjects
{
    public class ApiLogin
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait Wait;
        private readonly string url = "http://francoance.pythonanywhere.com/admin/login/";

        public ApiLogin(string browser)
        {
            this.driver = WebdriverFactory.GetWebdriver(browser);
            this.Wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(30));
            PageFactory.InitElements(this.driver, this);
        }

        [FindsBy(How = How.Id, Using = "id_username")]
        public IWebElement UsernameField { get; set; }

        [FindsBy(How = How.Id, Using = "id_password")]
        public IWebElement PasswordField { get; set; }

        [FindsBy(How = How.CssSelector, Using = "input[type='submit']")]
        public IWebElement SubmitButton { get; set; }

        public void GoTo()
        {
            this.driver.Navigate().GoToUrl(this.url);
        }

        public void QuitDriver()
        {
            this.driver.Quit();
        }

        public void InputUsername(string username)
        {
            this.UsernameField.Clear();
            this.UsernameField.SendKeys(username);
        }

        public void InputPassword(string password)
        {
            this.PasswordField.Clear();
            this.PasswordField.SendKeys(password);
        }

        public void PressSubmit()
        {
            this.SubmitButton.Click();
        }

        public bool isAtLogin()
        {
            if (this.driver.Title == "Identificarse | NUTRIR")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool isAtHome()
        {
            if (this.driver.Title == "Administración | NUTRIR") {
                return true;
            } else
            {
                return false;
            }
        }

    }
}
