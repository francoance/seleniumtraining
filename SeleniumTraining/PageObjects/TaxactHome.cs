﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;

namespace SeleniumTraining.PageObjects
{
    public class TaxactHome
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait Wait;
        private readonly string url = "https://aws-test.taxact.com";

        public TaxactHome(string browser)
        {
            this.driver = WebdriverFactory.GetWebdriver(browser);
            this.Wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(30));
            PageFactory.InitElements(this.driver, this);
        }

        [FindsBy(How = How.Id, Using = "prepare_file_taxes")]
        [CacheLookup]
        public IWebElement PrepareAndFileTaxesMenu { get; set; }

        [FindsBy(How = How.Id, Using = "gtm-products-container")]
        public IWebElement OnlineProductsContainer { get; set; }

        [FindsBy(How = How.Id, Using = "gtm-wizard-online-life-events")]
        public IWebElement LifeEvents { get; set; }

        [FindsBy(How = How.Id, Using = "sign-in-desk")]
        public IWebElement SignInButton { get; set; }

        public void GoTo()
        {
            this.driver.Navigate().GoToUrl(url);
        }

        public void QuitDriver()
        {
            this.driver.Quit();
        }

        public void GoToTaxesOnline()
        {
            PrepareAndFileTaxesMenu.Click();
            var OnlineProductsItem = this.Wait.Until<IWebElement>((d) =>
            {
                return d.FindElement(By.Id("online-products-header"));
            }
            );
            OnlineProductsItem.Click();
        }

        public void SelectLifeEventInTaxesOnline(string lifeEvent)
        {
            var LifeEventItem = LifeEvents.FindElement(By.Id(lifeEvent));
            if (!LifeEventItem.GetAttribute("class").Contains("active")){
                LifeEventItem.Click();
            }
        }

        public string GetRecommendedProduct()
        {
            var recommendedProd = OnlineProductsContainer.FindElement(By.ClassName("recommended"));
            return recommendedProd.GetAttribute("id");
        }

        public void DeselectLifeEventInTaxesOnline(string lifeEvent)
        {
            var LifeEventItem = LifeEvents.FindElement(By.Id(lifeEvent));
            if (LifeEventItem.GetAttribute("class").Contains("active"))
            {
                LifeEventItem.Click();
            }
        }

        public bool IsThereRecommendedProductInTaxesOnline()
        {
            return OnlineProductsContainer.FindElements(By.ClassName("recommended")).Count > 0;
        }

        public void GoToAuth() {
            SignInButton.Click();
        }
    }
}
