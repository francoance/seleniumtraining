﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SeleniumTraining.PageObjects
{
    public class StudentsSelect
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait Wait;
        private readonly string url = "http://uitestpractice.com/Students/Select";
        private SelectElement selector { get; set; }

        public StudentsSelect(string browser)
        {
            this.driver = WebdriverFactory.GetWebdriver(browser);
            this.Wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(30));
            PageFactory.InitElements(this.driver, this);
        }

        [FindsBy(How = How.Id, Using = "countriesSingle")]
        public IWebElement SingleSelect { get; set; }

        [FindsBy(How = How.Id, Using = "countriesMultiple")]
        public IWebElement MultiSelect { get; set; }

        [FindsBy(How = How.Id, Using = "dropdownMenu1")]
        public IWebElement Dropdown { get; set; }

        public void GoTo()
        {
            this.driver.Navigate().GoToUrl(url);
        }

        public void QuitDriver()
        {
            this.driver.Quit();
        }

        public void SelectSingle(string value)
        {
            selector = new SelectElement(SingleSelect);
            selector.SelectByText(value);
        }

        public string GetSingleSelectValue()
        {
            return selector.SelectedOption.Text;
        }

        public void SelectMulti(List<string> values)
        {
            selector = new SelectElement(MultiSelect);
            foreach (var value in values)
            {
                selector.SelectByText(value);
            }
        }

        public List<string> GetMultiSelectValues()
        {
            var selectedValues = selector.AllSelectedOptions.ToArray();
            var valuesList = new List<string>();
            foreach (var value in selectedValues)
            {
                valuesList.Add(value.Text);
            }
            return valuesList;
        }

        public void SelectDropdown(string value)
        {
            Dropdown.Click();
            var dropdownMenu = this.Wait.Until<IWebElement>((d) =>
            {
                return d.FindElement(By.ClassName("dropdown-menu"));
            }
            );
            var dropdownItem = dropdownMenu.FindElement(By.LinkText(value));
            dropdownItem.Click();
        }

        public string GetDropdownValue()
        {
            return Dropdown.Text;
        }

    }
}
