﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTraining.PageObjects
{
    public class ApiHome
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait Wait;
        private readonly string url = "http://francoance.pythonanywhere.com/admin/";

        public ApiHome(string browser)
        {
            this.driver = WebdriverFactory.GetWebdriver(browser);
            this.Wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(30));
            PageFactory.InitElements(this.driver, this);
        }

        public void GoTo()
        {
            this.driver.Navigate().GoToUrl(url);
        }

        public void QuitDriver()
        {
            this.driver.Quit();
        }

        [FindsBy(How = How.Id, Using = "user-tools")]
        public IWebElement UserTools { get; set; }

        public string GetUserToolsContent()
        {
            return UserTools.Text;
        }
    }
}
