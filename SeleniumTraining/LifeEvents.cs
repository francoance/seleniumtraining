﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTraining
{
    public static class LifeEvents
    {
        public const string JOB = "have_job";
        public const string CHILD = "children_dependents";
        public const string HOME = "own_a_home";
        public const string INVESTMENT = "own_rental_property";
        public const string SELFEMPLOYED = "self_employed";

        public static string[] Events = { JOB, CHILD, HOME, INVESTMENT, SELFEMPLOYED };
    }
}
