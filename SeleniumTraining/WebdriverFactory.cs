﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace SeleniumTraining
{
    public static class WebdriverFactory
    {
        public static IWebDriver GetWebdriver(string browser)
        {
            if (browser.Equals("Firefox"))
            {
                return new FirefoxDriver();
            }

            return new FirefoxDriver();
        }
    }
}
