﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTraining
{
    public static class OnlineProducts
    {
        public const string FREE = "free";
        public const string BASIC = "basic";
        public const string DELUXE = "deluxe";
        public const string PREMIER = "premier";
        public const string SELFEMPLOYED = "self_emp";

        public static string[] Products = { FREE, BASIC, DELUXE, PREMIER, SELFEMPLOYED };
    }
}
