﻿Feature: Create a new account
	In order to use the website
	As a user
	I want to create a new account

@web
Scenario: Add two numbers
	Given I am on the webpage
	When I go to the SignInButton
	And I select create a new Account
	Then I fill in the @email
	And I go to Verify Button 
	And I fill in all the boxes
	And I fill in all the security questions and secrete answers
	And I click on the Next Button
	And I click Skip for Now Button
	Then I validate against the account name
