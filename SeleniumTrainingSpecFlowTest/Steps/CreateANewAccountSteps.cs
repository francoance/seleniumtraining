﻿using NUnit.Framework;
using SeleniumTraining.PageObjects;
using System;
using TechTalk.SpecFlow;

namespace SeleniumTrainingSpecFlowTest.Steps
{
    [Binding]
    public class CreateANewAccountSteps
    {
        TaxactHome taxacthome;
        TaxactAuth taxactauth;
        Mailinator mailinator;
        string username;

        private readonly string browser = "Firefox";

        [Given(@"I am on the webpage")]
        public void GivenIAmOnTheWebpage()
        {
            taxacthome = new TaxactHome(browser);
        }
        
        [When(@"I go to the SignInButton")]
        public void WhenIGoToTheSignInButton()
        {
            taxacthome.GoToAuth();
            taxactauth = new TaxactAuth(browser);
        }
        
        [When(@"I select create a new Account")]
        public void WhenISelectCreateANewAccount()
        {
            taxactauth.SelectCreateNewAccountButton();
        }
        
        [Then(@"I fill in the @email")]
        public void ThenIFillInTheEmail()
        {
            string mail = "prueba@mailinator.com";
            taxactauth.FillEmail(mail);
        }
        
        [Then(@"I go to Verify Button")]
        public void ThenIGoToVerifyButton()
        {
            taxactauth.SelectNextButton();
        }
        
        [Then(@"I fill in all the boxes")]
        public void ThenIFillInAllTheBoxes()
        {
            //get code from mailinator
            string code = "123456";
            taxactauth.FillValidationCode(code);
            taxactauth.SelectVerifyButton();
        }
        
        [Then(@"I fill in all the security questions and secrete answers")]
        public void ThenIFillInAllTheSecurityQuestionsAndSecreteAnswers()
        {
            string answer = "a";
            taxactauth.FillFirstSecurityAnswer(answer);
            taxactauth.FillSecondSecurityAnswer(answer);
            taxactauth.FillThirdSecurityAnswer(answer);
        }
        
        [Then(@"I click on the Next Button")]
        public void ThenIClickOnTheNextButton()
        {
            taxactauth.SelectNextButton();
            taxactauth.SelectAcceptButton();
        }
        
        [Then(@"I click Skip for Now Button")]
        public void ThenIClickSkipForNowButton()
        {
            taxactauth.SelectSkipforNowLink();
        }
        
        [Then(@"I validate against the account name")]
        public void ThenIValidateAgainstTheAccountName()
        {
            var loggedinuser = taxactauth.GetLoggedInUser();
            Assert.AreEqual(username, loggedinuser);
        }
    }
}
